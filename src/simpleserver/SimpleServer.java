/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleserver;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 *
 * @author pfb1
 */
public class SimpleServer {
    
    int port;
    InetAddress ipAdress = null;
    ServerSocket serverSocket = null;
    
    public SimpleServer(Boolean useLoopback, int port) throws UnknownHostException
    {
        this.port = port;
        this.ipAdress = useLoopback ? Inet4Address.getLoopbackAddress() : Inet4Address.getLocalHost();
    }
    
    public void Start()
    {
        try {
            serverSocket = new ServerSocket(port, 1, ipAdress);
        } catch (IOException ex) {
            System.out.println("Failed to start listening");
            return;
        }
        
        System.out.println("Listening...");
        
        Socket socket;
        try {
            socket = serverSocket.accept();
        } catch (IOException ex) {
            System.out.println("Failed to accept connection");
            return;
        }
        
        System.out.println("Connection Made");
        
        try {
            SocketMethod(socket);
        } catch (IOException ex) {
            System.out.println("Failed to send/recieve data");
        }
    }
    
    public void Stop()
    {
        try { 
            serverSocket.close();
        } catch (IOException ex) {
            System.out.println("Failed to stop listening");
        }
    }
    
    private void SocketMethod(Socket socket) throws IOException
    {
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        Scanner in = new Scanner(socket.getInputStream());
        String inputLine, outputLine;

        out.println("Send 0 for available options");
        
        while ((inputLine = in.nextLine()) != null) 
        {
             System.out.println("Received...");
             
             int i;
             
             try
             {
                i = Integer.parseInt(inputLine);
             }
             catch (NumberFormatException e)
             {
                 i = -1;
             }
             
             out.println(GetReturnMessage(i));
             
             if (i == 9) 
             {
                break;
             }
        }
        
        out.close();
        in.close();
        socket.close();
    }
    
    private String GetReturnMessage(int code)
    {
        String returnMessage;

        switch (code)
        {
            case 0:
                returnMessage = "Send 1, 3, 5 or 7 for a joke. Send 9 to close the connection.";
                break;
            case 1:
                returnMessage = "What dog can jump higher than a building? Send 2 for punchline!";
                break;
            case 2:
                returnMessage = "Any dog, buildings can't jump!";
                break;
            case 3:
                returnMessage = "When do Ducks wake up? Send 4 for punchline!";
                break;
            case 4:
                returnMessage = "At the Quack of Dawn!";
                break;
            case 5:
                returnMessage = "How do cows do mathematics? Send 6 for punchline!";
                break;
            case 6:
                returnMessage = "They use a cow-culator.";
                break;
            case 7:
                returnMessage = "How many programmers does it take to screw in a light bulb? Send 8 for punchline!";
                break;
            case 8:
                returnMessage = "None, that's a hardware problem.";
                break;
            case 9:
                returnMessage = "Bye!";
                break;
            default:
                returnMessage = "Invalid Selection";
                break;
        }

        return returnMessage;
    }
}
