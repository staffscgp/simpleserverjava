/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleserver;

import java.net.UnknownHostException;

/**
 *
 * @author pfb1
 */
public class Program {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        SimpleServer simpleServer;
        try {
            simpleServer = new SimpleServer(true, 4444);
        } catch (UnknownHostException ex) {
            System.out.println("Unable to create server");
            return;
        }
        
        try
        {
            simpleServer.Start();
            simpleServer.Stop();
        }
        catch (Exception e)
        {
            System.out.println("Unexpected Exception: " + e.getMessage());
        }
    }
}
