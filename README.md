# README #

### What is this repository for? ###

* A Simple Networked Server Application. Used to demonstrate basic techniques such as opening a TCP Connection and reading/writing data.
* v1.0

### How do I get set up? ###

* Built in Java using Netbeans 7.2.1

### Who do I talk to? ###

* Paul Boocock - Staffordshire University
* paul.boocock@staffs.ac.uk